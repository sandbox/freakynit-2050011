
-- SUMMARY --

Freakynit module module which lets you embed the Javascript integration
code in to your pages without editing templates.
Install the module once. No need to change or write a single line of
code to make it work. Ever!

With this module your website becomes feedback ready immediately!

For a full description of the module, visit the project page:
-------------------------------------------------------------
    https://drupal.org/node/2050019


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
