<?php
/**
 * @file
 * main callback view
 *
 * @category view
 * @package   WebEngage
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.webengage.com/
 */

 /**
  * Implements hook_preprocess_HOOK().
  */
  function webengage_preprocess_webengage_main(&$vars) {
    $vars['m_license_code_old'] = getLicenseCode();
    $vars['m_widget_status'] = getWidgetStatus();
    $vars['message'] = isset($_REQUEST['message']) ? $_REQUEST['message'] : "";
    $vars['error_message'] = isset($_REQUEST['error-message']) ? $_REQUEST['error-message'] : "";
  }

 /**
  * Callback for main path.
  *
  * @return string
  *   the theme block
  */
  function doActionWebEngageMain() {
      return theme('webengage_main',
      array('m_license_code_old' => NULL,
      'm_widget_status' => NULL,
      'message' => NULL,
      'error_message' => NULL));
  }
