<?php
/**
 * @file
 * callback view
 *
 * @category view
 * @package   WebEngage
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.webengage.com/
 */

 /**
  * Implements hook_preprocess_HOOK().
  */
  function webengage_preprocess_webengage_callback(&$vars) {

  }

 /**
  * Callback for callback path.
  *
  * @return string
  *   the theme block
  */
  function doActionWebEngageCallback() {
      return theme('webengage_callback', array());
  }
