<?php
/**
 * @file
 * request processor
 *
 * @category helper
 * @package   WebEngage
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.webengage.com/
 */

 /**
  * Main request processor.
  */
  function process_webengage_options () {
    global $base_root;
    $redirect_url = "";
    $main_url = $base_root . base_path() . '?q=admin/webengage/action/main&noheader=true';

    if(isset($_REQUEST['weAction'])) {
  if ($_REQUEST['weAction'] === 'wp-save') {
      $message = update_webengage_options();
      $redirect_url = $main_url . '&' . $message[0] . "=" . urlencode($message[1]);
  } elseif ($_REQUEST['weAction'] === 'reset') {
      $message = reset_webengage_options();
      $redirect_url = $main_url . '&' . $message[0] . "=" . urlencode($message[1]);
  } elseif ($_REQUEST['weAction'] === 'activate') {
      $message = activate_we_widget();
      $redirect_url = $main_url . '&' . $message[0] . "=" . urlencode($message[1]);
  } elseif ($_REQUEST['weAction'] === 'discardMessage') {
      discard_status_message();
      $redirect_url = $main_url;
  }

  if(strlen($redirect_url) > 0) {
    drupal_goto($redirect_url);
  }
    }
  }

 /**
  * Discard message processor.
  */
  function discard_status_message () {
    setWidgetStatus('');
  }

 /**
  * Resetting processor.
  */
  function reset_webengage_options() {
    setLicenseCode('');
    setWidgetStatus('');
    return array("message", "Your WebEngage options are deleted. You can signup for a new account.");
  }

 /**
  * Update processor.
  */
  function update_webengage_options() {
    $wlc = isset($_REQUEST['webengage_license_code']) ? $_REQUEST['webengage_license_code'] : "";
    $vm = isset($_REQUEST['verification_message']) ? $_REQUEST['verification_message'] : "";
    $wws = isset($_REQUEST['webengage_widget_status']) ? $_REQUEST['webengage_widget_status'] : "ACTIVE";
    if(!empty($wlc)) {
      setLicenseCode(trim($wlc));
      setWidgetStatus($wws);
      $msg = !empty($vm) ? $vm : "Your WebEngage widget license code has been updated.";
      return array("message", $msg);
    } else {
      return array("error-message", "Please add a license code.");
    }
  }

 /**
  * Activate processor.
  */
  function activate_we_widget() {
  $wlc = isset($_REQUEST['webengage_license_code']) ? $_REQUEST['webengage_license_code'] : "";
  $old_value = getLicenseCode();
  $wws = isset($_REQUEST['webengage_widget_status']) ? $_REQUEST['webengage_widget_status'] : "ACTIVE";
    if ($wlc === $old_value) {
      setWidgetStatus($wws);
      $msg = "Your plugin installation is complete. You can do further customizations from your WebEngage dashboard.";
      return array("message", $msg);
    } else {
      $msg = "Unauthorized plugin activation request";
      return array("error-message", $msg);
    }
  }
