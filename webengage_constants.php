<?php
/**
 * @file
 * constants file
 *
 * @category constants
 * @package   WebEngage
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.webengage.com/
 */

  define("PATH_MAIN", "admin/webengage/action/main");
  define("PATH_CALLBACK", "admin/webengage/action/callback");
  define("PATH_RESIZE", "admin/webengage/action/resize");
