<?php
/**
 * @file
 * resize callback
 *
 * @category view
 * @package   WebEngage
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.webengage.com/
 */

 /**
  * Implements hook_preprocess_HOOK().
  */
  function webengage_preprocess_webengage_resize(&$vars) {

  }

 /**
  * Callback for resize path.
  *
  * @return string
  *   the theme block
  */
  function doActionWebEngageResize() {
      return theme('webengage_resize', array());
  }
