<?php
/**
 * @file
 * the callback view
 *
 * @category view
 * @package   WebEngage
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.webengage.com/
 */
  module_load_include("php", "webengage", "webengage_constants");
  global $base_root;
  $main_url = $base_root . base_path() . '?q=' . PATH_MAIN . '&noheader=true';

  $wlc = isset($_REQUEST['webengage_license_code']) ? $_REQUEST['webengage_license_code'] : "";
  $vm = isset($_REQUEST['verification_message']) ? $_REQUEST['verification_message'] : "";
  $wwa = isset($_REQUEST['webengage_widget_status']) ? $_REQUEST['webengage_widget_status'] : "";

  if(isset($wlc)){
    ?>
    <html>
      <body>
        <form id="postbackToWp" name="postbackToWp" method="post" action="<?php echo $main_url . '&option=' . $_REQUEST['option']; ?>" target="_top">
          <input type="hidden" name="weAction" value="wp-save"/>
          <input type="hidden" name="verification_message" value="<?php echo urlencode($vm); ?>"/>
          <input type="hidden" name="webengage_license_code" value="<?php echo $wlc; ?>"/>
          <input type="hidden" name="webengage_widget_status" value="<?php echo $wwa; ?>"/>
        </form>
        <script type="text/javascript">
          //document.getElementById("postbackToWp").setAttribute("action", parent.parent.window.location.href + "&noheader=true");
          document.getElementById("postbackToWp").submit();
        </script>
      </body>
    </html>
  <?php
  }
